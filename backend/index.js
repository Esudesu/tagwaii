const express = require('express')
const app = express()
const mysql = require('mysql2')
const cors = require('cors')
const bodyParser = require('body-parser')
const bcrypt = require('bcrypt')
const saltRounds = 10
const cookieParser = require('cookie-parser')
const session = require('express-session')



const db = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'tagwaii',
  port: 42069,
})

app.use(express.json())
app.use(cors({
  origin: ["http://localhost:3000"],
  methods: ["GET", "POST"],
  credentials: true,
}))
app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(session({
  key: "UserID",
  secret: "jakisnibysekretnyklucz",
  resave: false,
  saveUninitialized: false,
  cookie: {
    expires: 60* 60 * 60 * 24,
  }
}))

app.get('/users', (req, res) => {
  const sqlSelect = "SELECT Login FROM users"

  db.query(sqlSelect, (err, result) => {
    res.send(result)
  })
})

app.post('/userRegister', (req, res) => {
  console.log('dziala')
  const login = req.body.loginData.login
  const password = req.body.loginData.password
  console.log(req.body)
  const sqlInsert = "INSERT INTO users (Login,Password) VALUE (?,?)"

  //if (err) console.log(err)
  bcrypt.hash(password, saltRounds, (err, hash) => {
    if (err) console.log(err)
    db.query(sqlInsert, [login, hash], (err, result) => {
      console.log(result)
      console.log(err)
      if (err) res.send(err)
      else res.send(result)
    })
  })



});

app.get("/endSession", (req, res,err) => {
  req.session.destroy()
  res.send({ message: "session ended" })
  console.log(err)
})
app.get('/checkSession', (req, res) => {
  if (req.session.user) {
    res.send({ loggedIn: true, user: req.session.user })
  }
  else {
    res.send({ loggedIn: false })
  }
})

app.post('/userLogin', (req, res) => {
  const login = req.body.loginData.login
  const password = req.body.loginData.password
  const userInDatabase = "SELECT * FROM users WHERE Login =?"
  //console.log(req.body)

  db.query(userInDatabase, login, (err, result) => {
    //if not exist user res.send(usr does not exist)
    //if not password wrong res.send(pass wrong)
    //if all matched 
    /*
      add new hash to session list
      res.send (hash)
    */
    /*const usrLst = result
    //const userID = usrLst[0].UserID
    if (usrLst.length > 0) {
      const userID = usrLst[0].UserID
      res.send({ wszystkochcepakowacwjson: userID })
    }
    else {
      console.log("user not exists")
    }*/
    if (err) console.log(err)
    if (result.length > 0) {
      bcrypt.compare(password, result[0].Password, (err, response) => {
        if (response) {
          req.session.user = result
          console.log(req.session.user)
          res.send({ login: result[0].Login })
        }
        else {
          res.send({ error: "wrong username or password" })
        }
      })
    }
    else {
      res.send({ error: "User does not exist" })
    }



  })


})
app.get('/user/info/getUserID', (req, res) => {
  if (req.session.user) res.send({ UserName: req.session.user[0].Login, UserID: req.session.user[0].UserID })
  else res.send("you are not logged in")
})

app.post('/user/info/update', (req, res, err) => {
  console.log('dziala')
  const UserID = req.session.user[0].UserID
  const Name = req.body.userData.name
  const Surname = req.body.userData.surname
  const Email = req.body.userData.email
  console.log(req.body)
  const sqlUpdate = "UPDATE user_data SET Name = ? , Surname = ? , Email = ? WHERE UserID = ? ;"

  //if (err) console.log(err)
  if (err) console.log(err)
  db.query(sqlUpdate, [Name, Surname, Email, UserID], (err, result) => {
    console.log(result)
    console.log(err)
    if (err) res.send(err)
    else res.send(result)
  })

})
app.get('/user/info/indatabase', (req, res, err) => {
  const userId = req.session.user[0].UserID
  const sqlFind = "SELECT * FROM user_data WHERE UserID = ?;"
  db.query(sqlFind, [userId], (err, result) => {
    console.log(result)
    res.send(result)
  })
})


app.get('/user/info/profile', (req, res, err) => {
  const userLogin = req.query.login
  const sqlFind = "SELECT UserID FROM users WHERE Login = ?;"
  var userId

  db.query(sqlFind, [userLogin], (err, result) => {
    if (result.length > 0) {
      userId = result[0].UserID
      console.log(userId)

      const sqlFindUserData = "SELECT * FROM user_data WHERE UserID = ?;"

      db.query(sqlFindUserData, [userId], (err, result) => {
        console.log("siema")
        console.log(result)
        res.send(result)
      })
    }
    else {
      res.send("user does not exist")
    }
  })
  //console.log(userId)
})

//frontend->backend->frontend->backend->frontend 

app.get('/user/info/getUserID', (req, res) => {
  if (req.session.user) res.send({ UserName: req.session.user[0].Login, UserID: req.session.user[0].UserID })
  else res.send("you are not logged in")
})

app.post('/user/info/update', (req, res, err) => {
  console.log('dziala')
  if (req.session.user) {
    const UserID = req.session.user[0].UserID
    const Name = req.body.userData.name
    const Surname = req.body.userData.surname
    const Email = req.body.userData.email
    console.log(req.body)
    const sqlUpdate = "UPDATE user_data SET Name = ? , Surname = ? , Email = ? WHERE UserID = ? ;"

    //if (err) console.log(err)
    if (err) console.log(err)
    db.query(sqlUpdate, [Name, Surname, Email, UserID], (err, result) => {
      console.log(result)
      console.log(err)
      if (err) res.send(err)
      else res.send(result)
    })
  }
  else res.send({ message: "you are not logged in" })
})
app.get('/user/info/indatabase', (req, res, err) => {
  if (req.session.user) {
    const userId = req.session.user[0].UserID
    const sqlFind = "SELECT * FROM user_data WHERE UserID = ?;"
    db.query(sqlFind, [userId], (err, result) => {
      console.log(result)
      res.send(result)
    })
  }
  else res.send("you are not logged in")
})


app.get('/user/info/profile', (req, res, err) => {
  const userLogin = req.query.login
  const sqlFind = "SELECT UserID FROM users WHERE Login = ?;"
  var userId

  db.query(sqlFind, [userLogin], (err, result) => {
    if (result.length > 0) {
      userId = result[0].UserID
      console.log(userId)

      const sqlFindUserData = "SELECT * FROM user_data WHERE UserID = ?;"

      db.query(sqlFindUserData, [userId], (err, result) => {
        console.log("siema")
        console.log(result)
        res.send(result)
      })
    }
    else {
      res.send("user does not exist")
    }
  })
  //console.log(userId)
})

app.post("/user/Follow", (req, res) => {
  const userFollower = req.session.user[0].UserID
  const userProfile = req.body.userId
  const sqlFollow = "INSERT INTO follows (FollowerID,FollowingID) VALUES (?,?);"

  db.query(sqlFollow, [userFollower, userProfile], (err, result) => {
    console.log(result)
    res.send(result)
    console.log(err)
  })
})

app.post("/user/Unfollow", (req, res) => {
  const userFollower = req.session.user[0].UserID
  const userProfile = req.body.userId
  const sqlUnfollow = "DELETE FROM follows WHERE FollowerID = ? AND FollowingID = ?;"

  db.query(sqlUnfollow, [userFollower, userProfile], (err, result) => {
    res.send(result)
  })
})


app.get("/checkFollow", (req, res) => {
  const sqlCheckFollow = "SELECT * FROM follows WHERE FollowerID = ? AND FollowingID = ?;"
  const userFollower = req.session.user[0].UserID
  const userProfile = req.query.userId

  db.query(sqlCheckFollow, [userFollower, userProfile], (err, result) => {
    //console.log(result)

    res.send(result)
  })
})

app.get("/endSession", (req, res) => {
  req.session.destroy()
  res.send({ message: "session ended" })
})

app.get("/home/getinfo", (req, res) => {
  if (req.session.user) {
    const user = req.session.user[0].UserID
    res.send({ userid: user })

  }
})


/*app.delete('/api/delete/:login', (req, res) => {
  const login = req.params.login
  const sqlDelete = "DELETE FROM users WHERE Login =?";

  db.query(sqlDelete, login, (err, result) => {
    if (err) console.log(err);
  })
})*/

app.listen(3001, () => {
  console.log("running on port 3001");
})