import React, { Component, useEffect, useState } from 'react';
import './index.css';
import axios from "axios"
import './basetheme.css'
import './darktheme.css'
import './lighttheme.css'
import { useParams } from 'react-router-dom';

axios.defaults.withCredentials = true

function UserPage() {

    const [userSession, setUserSession] = useState('')
    const [followers, setFollowers] = useState('')
    const [following, setFollowing] = useState('')
    const [userId, setUserId] = useState('')
    const [btnStyle, setBtnStyle] = useState('')
    const [btnVal, setBtnVal] = useState("Follow")
    var { username } = useParams()
    axios.get("http://localhost:3001/checkSession").then((response) => {
        setUserSession(response.data.loggedIn)
    })
    axios.get("http://localhost:3001/user/info/profile", {
        params: {
            login: username
        }
    }).then(res => {
        if (res !== "user does not exist") {
            //console.log(res.data[0].Following)
            setFollowing(res.data[0].Following)
            setFollowers(res.data[0].Followers)
            setUserId(res.data[0].UserID)
        }

    })
    if (userSession) {
        axios.get("http://localhost:3001/checkFollow", {
            params: {
                userId: userId
            }
        }).then(res => {
            if (res.data.length > 0) {
                setBtnVal("Unfollow")
            }
        })


        const handleFollow = (event) => {

            axios.post("http://localhost:3001/user/Follow", { userId }).then(res => {
                console.log(res)

            })
        }
        const handleUnfollow = (event) => {

            axios.post("http://localhost:3001/user/Unfollow", { userId }).then(res => {
                console.log(res)

            })
        }
        var handleButton = event => {
            if (btnVal === "Follow") {
                handleFollow()
            }
            else {
                handleUnfollow()
            }
        }
    }
    if (followers !== undefined) {
        if (userSession === true) {
            return (
                <div>
                    <h1>{username}</h1>
                    <h5>Following:{following}</h5>
                    <h5>Followers:{followers}</h5>
                    <button onClick={handleButton} >{btnVal}</button>
                </div>
            )
        }
        else {
            return (
                <div>
                    <h1>{username}</h1>
                    <h5>Following:{following}</h5>
                    <h5>Followers:{followers}</h5>

                </div>
            )
        }
    }
    else {
        return (
            <div>User not exists</div>
        )
    }


}

export default UserPage;
