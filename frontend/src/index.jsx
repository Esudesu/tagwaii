import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Cookies from 'universal-cookie';
import './basetheme.css'
import './darktheme.css'
import './lighttheme.css'
import axios from 'axios';
import UserInfo from './userPage.jsx';
import {BrowserRouter as Router, Switch, Route, Link, Redirect} from 'react-router-dom';
import MainPage from './main.jsx'
axios.defaults.withCredentials = true
const cookies = new Cookies();
class ManageScreens extends React.Component {

    constructor(props) {
        super(props);
        
       
    }
    
    // componentDidMount(){
    //     this.
    // }
    render() {
        return (
            <Switch>
                <BackgroundManager />
            </Switch>
        )
    }
}

class BackgroundManager extends React.Component {

    constructor(props) {
        super(props);
        var onloadTheme = localStorage.getItem('selectedTheme')
            
            this.state = {
                loggedIn:false,
                theme: (onloadTheme!==undefined ? onloadTheme:'basetheme'),
                login: ''
            }
        

        this.backgroundChange = this.backgroundChange.bind(this);


    }
    componentDidMount(){
        this.checkSession()
    }
    componentDidUpdate(prevProps,prevState){
        if(this.state.loggedIn !== prevState.loggedIn){
            this.checkSession();
        }
    }
    successfulLoginHandle = (isSuccessful, login) => {
        
        this.setState({ loggedIn: isSuccessful, login: login })
        
    }
    logoutHandle = () => {
        cookies.set('loggedIn',false)
        this.setState({loggedIn:false, login: ''})
        

    }
    backgroundChange = (backgroundType) => {
        this.setState({ background: backgroundType })
        //"registerScreenBackground"
    }
    changeTheme = themeNumber => {
        var selectedTheme = {
            0:'basetheme',
            2:'darktheme',
            1:'lighttheme'
        }[themeNumber]


        this.setState({ theme: selectedTheme });
        localStorage.setItem('selectedTheme', this.state.theme)

    }

    getThemeNumber = (themeName) => {

        return (
            {
                basetheme:0,
                darktheme:2,
                lighttheme:1,
                undefined:0
            }[themeName]
        )
    }
    
    
    checkSession = () => {
        axios.get("http://localhost:3001/checkSession").then((response) => {
            if (response.data.loggedIn !== false){
               this.setState({loggedIn:response.data.loggedIn, login:response.data.user[0].Login})
            }
            
        })
    }
    checkLogin = () => {
        axios.get("http://localhost:3001/checkSession").then((response) => {
            if (response.data.loggedIn === false){
                //this.props.location.pathname = '/';
                this.setState({loggedIn:false, login:''})
            }
            
        })
    }
    endSession = () => {
        axios.get("http://localhost:3001/endSession").then((res) => {
            console.log(res)
        })
        this.setState({loggedIn:false});
    }
   
    
     render(){
        
        //let {login} = this.params;
        
        var className = "theme_" + this.state.theme;
        localStorage.setItem('selectedTheme', this.state.theme);
        return (
            
            <div id='managedBackground' className={className}>
                <nav className="navbar navbar-dark bg-dark fixed-top">
                    <Link to='/home' className='navbar-brand'>Tagwaii</Link>
                    <NavbarContent location={this.props.location} params={this.state}/>
                </nav>
                
                <Route exact path='/' render= {() => (!this.state.loggedIn ? <Redirect to='login'/> : <Redirect to='home'/>)}/>
                <Route path='/login' render={props=>(!this.state.loggedIn ? <LoginScreenWidget SuccessfulLogin={this.successfulLoginHandle} {...props}/> : <Redirect to='/home'/>)}/>
                <Route path='/register' render={props=>(!this.state.loggedIn ? <RegisterScreenWidget theme={this.state.theme} {...props}/>: <Redirect to='/home'/>)}/>
                <Route exact path='/profile' render={() => (!this.state.loggedIn ? <Redirect to='/login'/> : <Redirect to={`/profile/${this.state.login}`}/>)}/>
                <Route path='/home' render={() => (!this.state.loggedIn ? <Redirect to='login'/> : <MainPage/>)}/>
                <Route path="/profile/:login" render={(props) => <UserInfo {...props} checkLogin={this.checkLogin}/>}/>

                <input type="range" min="0" max="2" step="1" id="themeSelector" defaultValue={this.getThemeNumber(this.state.theme)} onChange={(e) => { this.changeTheme(e.target.value) }} ></input>
            </div>
            
        )

    }

}
class LoginScreenWidget extends React.Component {


   

    componentDidMount() {
        axios.get("http://localhost:3001/checkSession").then((response) => {
            console.log(response)
        })
    }
    handleBackgroundChange = (bgType) => {
        this.props.BackgroundChange(bgType)
    }
    handleSuccessfulLogin = (isSuccessful, login) => {
        cookies.set('loggedIn',isSuccessful)
        this.props.SuccessfulLogin(isSuccessful, login)

    }
    handleLogin = (login, password, event) => {
        event.preventDefault();

        const loginData = {
            login: login,
            password: password
        }
        axios.post('http://localhost:3001/userLogin', { loginData }).then(res => {

            //jezeli zwraca login to this.handlesuccessfullogin(true)

            if (loginData.login === res.data.login) {
                
                this.handleSuccessfulLogin(true, login);
                cookies.set('loggedIn',true,{maxAge: 10})
                this.props.history.push('/home')
            }
            else {
                
                alert("No matching user or password. Please try again");
                document.getElementById('passwordInput').value = '';
                document.getElementById('usernameInput').value = '';
            }
        });

    }

    
    formSubmission = event => {
        var username = document.getElementById('usernameInput');
        var password = document.getElementById('passwordInput');
        this.handleLogin(username.value,password.value,event);
        
        
    }
    
    render() {
        return (
            <div id="loginScreenWidget" className="shadow-lg rounded">
                <div id="loginWrapper" >
                    <div className="loginComponent">
                        <form id="loginForm" >
                        <label  id="usernameInputLabel">Username:</label>
                        <input className="form-control" id="usernameInput" placeholder="Type username or email address..."></input>
                        <label id="passwordInputLabel">Password:</label>
                        <input className="form-control" id="passwordInput" type="password" placeholder="Type your password..."></input>
                        <Link id="loginButton" className="btn btn-primary" onClick={this.formSubmission}>Log in on your Tagwaii account!!!</Link>
                        </form>
                    </div>

                    <div className="registerComponent">
                        <Link id="registerButton"  className="btn btn-primary" to='/register'>Create your own Tagwaii account!!!</Link>
                    </div>
                </div>
            </div>
        )
    }
}

class RegisterScreenWidget extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            user: '',
            password: '',
            repeatedPassword: ''
        }
    }

   
    handleSubmit = event => {

        event.preventDefault();
        var validUsername = 0;
        var validPassword = 0;
        var password = document.getElementById('passwordInput').value;
        var username = document.getElementById('usernameInput').value;
        if (username.length > 6) {
            validUsername = 1;
        }
        else {
            alert('Your username is too short')
            document.getElementById('usernameInput').value = '';
            document.getElementById('passwordInput').value = '';
            document.getElementById('repeatPasswordInput').value = '';
        }

       
        
        if (validUsername === 1){
        if (document.getElementById('passwordInput').value === document.getElementById('repeatPasswordInput').value) {
            if ((/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/).test(password)) {
                validPassword = 1;
    
            }
            else {
                password = "";
                document.getElementById('passwordInput').value = '';
                document.getElementById('repeatPasswordInput').value = '';
                alert("Password does not meet requirements")
            }
            if (validPassword === 1 ) {
                
    
    
    
            
            const loginData = {
                login: document.getElementById('usernameInput').value,
                password: document.getElementById('passwordInput').value
            }
            axios.post('http://localhost:3001/userRegister', { loginData }).then(res => {
                alert("Successfully registered. Have fun on Tagwaii!")
                this.props.history.push('/login');
            });
        }
        } else {
            password = "";
            document.getElementById('passwordInput').value = '';
            document.getElementById('repeatPasswordInput').value = '';
            alert("Password fields are not the same")
        }
    }
    }

    handleBackgroundChange = (bgType) => {
        this.props.BackgroundChange(bgType)
    }


    render() {

        var closeIconId = { basetheme: "baseCloseIcon", darktheme: "darkCloseIcon", lighttheme: "lightCloseIcon" }[this.props.theme]
        return (
            <div id="registerScreenWidget" className="shadow-lg rounded">
            <Link to='/login'>
                <svg xmlns="http://www.w3.org/2000/svg" id={closeIconId} width="15" height="15" className="bi bi-x-lg" viewBox="0 0 16 16">
                        <path d="M2.64,1.27L7.5,6.13l4.84-4.84C12.5114,1.1076,12.7497,1.0029,13,1c0.5523,0,1,0.4477,1,1  c0.0047,0.2478-0.093,0.4866-0.27,0.66L8.84,7.5l4.89,4.89c0.1648,0.1612,0.2615,0.3796,0.27,0.61c0,0.5523-0.4477,1-1,1  c-0.2577,0.0107-0.508-0.0873-0.69-0.27L7.5,8.87l-4.85,4.85C2.4793,13.8963,2.2453,13.9971,2,14c-0.5523,0-1-0.4477-1-1  c-0.0047-0.2478,0.093-0.4866,0.27-0.66L6.16,7.5L1.27,2.61C1.1052,2.4488,1.0085,2.2304,1,2c0-0.5523,0.4477-1,1-1  C2.2404,1.0029,2.4701,1.0998,2.64,1.27z" id="path2" />
                </svg>
            </Link>
                <div id="registerWrapper" >
                        
                        
                        
                        <form id="registerForm" onSubmit={this.handleSubmit}>
                        <label id="usernameInputLabel">Username:</label>
                        <input className="form-control" id="usernameInput" placeholder="Choose username..."></input>
                        <label id="passwordInputLabel">Password:</label>
                        <input className="form-control" id="passwordInput" type="password" placeholder="Type your password..."></input>
                        <label id="repeatPasswordInputLabel">Repeat password:</label>
                        <input className="form-control" id="repeatPasswordInput" type="password" placeholder="Repeat password"></input>
                        <button id="registerButton" className="btn btn-outline-primary" >Create your own Tagwaii account!!!</button>
                        </form>
                </div>
            </div>
        )
    }
}
class NavbarContent extends React.Component{
    render(){
        
       
        if(this.props.location.pathname === '/login' || this.props.location.pathname === '/register'){
            return(
                <div>You are on login screen</div>
            )
        }else {
            return(
                
                <ul>
                    <li>{
                        (this.props.params.loggedIn ? <Link to={`/profile/${this.props.params.login}`}>Your page</Link> : <Link to={'/login'}>Log in</Link>)
                    }
                    
                    </li>
                </ul>
                
                
            )
        }
    }
}
ReactDOM.render(
    <Router>
        <ManageScreens/>
    </Router>,
    document.getElementById('root')
)
