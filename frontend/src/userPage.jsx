import React, { Component } from 'react';
import './index.css';
import axios from "axios"
import './basetheme.css'
import './darktheme.css'
import './lighttheme.css'


axios.defaults.withCredentials = true

class userPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: "",
            userID: '',
            name: '',
            surname: '',
            email: '',
            NameDB: '',
            SurnameDB: '',
            EmailDB: '',
        }

    }

    componentDidMount() {
        this.userInfoInDB()
    }


    userInfoInDB = (login, password, event, res) => {
        var nameInput = document.getElementById("Name").value
        var surnameInput = document.getElementById("Surname").value
        var emailInput = document.getElementById("Email").value

        const userData = {
            name: nameInput,
            surname: surnameInput,
            email: emailInput
        }


        axios.get('http://localhost:3001/user/info/getUserID').then(res => {

            //jezeli zwraca login to this.handlesuccessfullogin(true)
            if (res.data !== "you are not logged in") this.setState({ login: res.data.UserName, userID: res.data.UserID })
            else this.setState({ login: res.data })
            console.log(res.data)
        });
        if (nameInput !== "" && surnameInput !== "" && emailInput !== "") {
            axios.post('http://localhost:3001/user/info/update', { userData }).then(res => {
                //res.send({ data: userData })
                console.log(userData)
            })
        }
        axios.get("http://localhost:3001/user/info/indatabase").then(res => {
            this.setState({ NameDB: res.data[0].Name, SurnameDB: res.data[0].Surname, EmailDB: res.data[0].Email })
        })

    }


    render() {
        if (this.state.login === "you are not logged in") {
            return (
                <div>
                    <h1>You are not logged in</h1>
                </div>
            )
        }
        else {
            return (
                <div>
                    <h1>{this.state.login}</h1>
                    <form>

                        <input id="Name" placeholder="Name" defaultValue={this.state.NameDB}></input>
                        <input id="Surname" placeholder="Surname" defaultValue={this.state.SurnameDB}></input>
                        <input id="Email" placeholder="Email" defaultValue={this.state.EmailDB}></input>
                        <button type="submit" onClick={this.userInfoInDB}>submit</button>
                    </form>
                </div>
            );
        }
    }
}




        
export default class UserInfo extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            username:'',
            following:'',
            followers:''
        }
        this.props.checkLogin()
        
    }
   
    getData() {
        
            axios.get("http://localhost:3001/users").then((res) => {
                console.log(res.data)
                res.data.map((user,index) => {
                    
                    if(user.Login === this.props.match.params.login){
                        console.log(user.Login)
                       axios.get("http://localhost:3001/user/info/profile", {
                        params: {
                            login: user.Login
                        }
                    }).then(res => {
                        
                            //console.log(res.data[0].Following)
                            this.setState({following:res.data[0].Following,followers:res.data[0].Followers,username:user.Login})
                            
                        
                
                    })
                    return;
                    }
                        
                    
                })
            })
    }
    componentDidMount(){
        this.getData();
    }
    componentWillUnmount(){
        this.props.checkLogin()
        
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props.match.params.login !== this.state.username){
            this.getData();
          }
    }
    
           
    render(){
       
        
      
            
    
    
        if(this.state.username!==''){
            return(
                
                <div>
                {this.state.username}{this.state.followers}{this.state.following}
                </div>
            )
        }
        return (
            <div>
                User {this.props.match.params.login} not found
            </div>
        )
        
        
    }
}
