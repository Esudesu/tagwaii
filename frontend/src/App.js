import React, { useImperativeHandle } from 'react';
//import ReactDOM from 'react-dom';
import './index.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    useParams,
    matchPath
    //Link
} from "react-router-dom";
import './basetheme.css'
import './darktheme.css'
import './lighttheme.css'
import { ManageScreens, } from './loginScreen.jsx';
import userPage from "./userPage"
import profilePage from "./profilePage"



function App() {

    return (
        <Router>
            <Switch>
                {/*<Route path="/" component/> */}
                <Route path="/userInfoPage" component={userPage} />
                <Route path="/loginScreen" component={ManageScreens} />
                <Route path="/profile/:username" component={profilePage} />


            </Switch>
        </Router>
    )

}

export default App;
