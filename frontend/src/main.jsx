import React from 'react';
import ReactDOM from 'react-dom';
import './main.css';
import Cookies from 'universal-cookie';
import './basetheme.css'
import './darktheme.css'
import './lighttheme.css'
import axios from 'axios';
import {Link} from 'react-router-dom'
export default class MainPage extends React.Component {
    render(){
        return(
            <div id='mainContainer'>
            <UserRecommendation/>
            <ScrollingContent/>
            <FreeArea/>
            </div>
        )
    }
}
class UserRecommendation extends React.Component {
    render(){
        return(
            <div id='userRecommendationContainer'>
                <UserColumn/>
            </div>


        )
    }
}
class UserColumn extends React.Component {
    render(){
        return(
           <ul id='userColumn'>
               <UserRow/>
           </ul>
            
        )
    }
}
class UserRow extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            users: []
        }
    }
    componentDidMount(){
        axios.get('http://localhost:3001/users').then(res => {
            this.setState({users:res.data})
        })
    }
    render(){
        return(
            this.state.users.map((user,index)=>{
                
                return (<li key={index} className='userRow'>
                            <Link to={`/profile/${user.Login}`}>{user.Login}</Link>
                        </li>)
            })
            
        )
    }
}
class FreeArea extends React.Component {
    render(){

        //animated logo, apearing muffins
        return(
        <div id='freeAreaContainer'></div>
        )
    }
}
class ScrollingContent extends React.Component {

    render(){
        return(
            <div id='scrollingZone'>
            <PostContainer/>
            </div>
        )
    }
}

class PostContainer extends React.Component {
    render(){
        return(
            <div className='postContainer'>
            <AuthorContainer/>
            <DisplayPicture/>
            <ActionComponents/>
            <CommentColumn/>
            </div>
        )
        //div
            //autor
            //img
            //actioncomponents (com, like)
            //comments
                //each individual comment div

    }
}
class AuthorContainer extends React.Component {
    render(){
        return(

        //author username //score
        <div className='authorContainer'></div>
        )
        
    }
}
class DisplayPicture extends React.Component {
    render(){
        return(
        //image container
        <div className='pictureContainer'></div>
        )
    }
}
class ActionComponents extends React.Component {
    render(){

        return(
        <div className='actionComponentsContainer'><button className='likeButton'></button><button className='commentButton'></button></div>
        )
        //button like
        //button com
        
    }
}
class CommentColumn extends React.Component {
    render(){
        return(
            //div and multiple comment rows
        <div className='commentColumn'>
        
            <CommentRow/>
        </div>
        )
    }
}
class CommentRow extends React.Component {
    render (){
        return(
        <div className='commentRow'></div>
        )
        //username, comment content
    }
}