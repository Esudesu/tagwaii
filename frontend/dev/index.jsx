import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class ManageScreens extends React.Component {
    
    constructor(props){
        super(props);
        this.backgroundTypeChange = this.backgroundTypeChange.bind(this)
        this.state = {
            screenType:"loginScreen"
        }
    }
    backgroundTypeChange(selectedBackground){
        
        this.setState({screenType:{selectedBackground}})
    }
    render(){
        return(
           <BackgroundManager backgroundTypeChange={this.backgroundTypeChange}/>
        )
    }
}
class BackgroundManager extends React.Component{
    
    constructor(props){
        super(props);
        this.state ={
            background:"loginScreenBackground"
        }
    }
    
     render(){
        return (
            <div id='managedBackground'>{
            
                {
                    loginScreenBackground: <LoginScreenWidget/>,
                    registerScreenBackground: <RegisterScreenWidget/>
                }[this.state.background]
                
            }<button onClick ={backgroundTypeChange("registerScreenBackground")}>Change background type </button>
            </div>
        )
     }
}
class LoginScreenWidget extends React.Component {
    render(){
        return(
            <div id="LoginScreenWidget"></div>
        )
    }
}
class RegisterScreenWidget extends React.Component {
    render(){
        return(
            <div id="RegisterScreenWidget"></div>
        )
    }
}
ReactDOM.render(
<ManageScreens/>,
document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
